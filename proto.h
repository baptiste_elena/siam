#ifndef PROTO_H_INCLUDED
#define PROTO_H_INCLUDED

#include <string>
#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <windows.h>

using namespace std;

class Elephant
{
private:

public:
};

class Rhino
{
private:

public:
};

class Animal : public Elephant, public Rhino
{
private:
    char m_direction;
    int m_force;

public:
    Animal(){};
    Animal(char _direction)
          : m_direction(_direction),m_force(10){};
    ~Animal(){};
    char getdir();
    void setdir(char direction);
    int getforce();
};

class Montagne
{
public:
    Montagne(){};
    ~Montagne(){};
} ;

class Pion : public Animal,public Montagne
{
private:
    int m_x;
    int m_y;
    int m_res;
    string m_nom;
    //int m_nombreA;

public:
    Pion(){};
    Pion(int _x, int _y,int _res,string _nom,char _direction)
         :Animal('o'),m_x(_x),m_y(_y),m_res(_res),m_nom(_nom){};
    ~Pion(){};
    int getx();
    int gety();
    int getres();
  /*  int getNbr();
    int setNbr(int nombreA);*/
    string getnom();
    int afficherPion(int &nbrE, int &nbrR, string &tour);
};

class Plateau
{
private:
    vector<Pion>m_vec;

public:
    Plateau(){m_vec.push_back(Pion(3,2,9,"M",'o'));
              m_vec.push_back(Pion(3,3,9,"M",'o'));
              m_vec.push_back(Pion(3,4,9,"M",'o'));};
    ~Plateau(){};
    void afficherElem(vector<Pion>vec, int &i, int &j);
    void insert_pion(Pion p);
    void delete_pion();
};

class Joueur
{
private:
    string m_J1,m_J2;
    Plateau m_p;
    char m_dir;
    bool m_test;

public:
    Joueur(){};
    Joueur(string _J1,string _J2,Plateau p,char _dir,bool _test)
           :m_J1(_J1),m_J2(_J2),m_dir(_dir),m_test(_test){};
    ~Joueur(){};
    string getJou1();
    string getJou2();
    char getdir();
    bool gettest();
    void entrerAnimal(Plateau p,int _x,int _y,char _dir,string _nom, bool _test);
    void deplacerAnimal(Plateau p,int _x,int _y,char _dir,string _nom, bool _test);
    void sortirAnimal(Plateau p,int _x,int _y,string _nom, bool _test);
    void modifierAnimal(Plateau p,int _x,int _y,char _dir,string _nom, bool _test);
};

class Partie
{
private:
    Joueur m_J1,m_J2;
    Plateau m_pl;
    int m_tour;

public:
    Partie(){};
    Partie(Plateau _pl,Joueur _J1,Joueur _J2,int _tour)
          :m_pl(_pl),m_J1(_J1),m_J2(_J2),m_tour(_tour){};
    ~Partie(){};
    int gettour();
    string settour(string& tour);
    int Jouer(int &i);
    int afficherTab(int& m);

};
    int tablo();

    void debut();
    int ssmenu1();
    int act1();
    int ssmenu2();
    int scoreAcces();
    int ssmenuSauvegarde();
    int choix_finjeu();
    int perdu1();
    int musique1();


#endif // PROTO_H_INCLUDED
